FROM maven:3-jdk-8-slim AS builder 
WORKDIR /app
COPY pom.xml .
RUN mvn dependency:go-offline
COPY src/ /app/src
RUN mvn package -DskipTests


FROM openjdk:11-jre-slim
WORKDIR /app
COPY --from=builder /app/target/*.jar /app/app.jar
EXPOSE 2222
CMD ["java", "-jar", "app.jar"]
